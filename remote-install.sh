#!/usr/bin/env sh

git clone https://github.com/ostera/dotfiles ~/repos/ostera/dotfiles
cd ~/repos/ostera/dotfiles
./bootstrap.sh
