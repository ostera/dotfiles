#!/bin/sh

# Tap 'em!
brew tap tldr-pages/tldr

# Brewable installs!

brew install \
  coreutils \
  dockutil \
  fzf \
  gettext \
  gettext \
  gist \
  git \
  git-extras \
  gnu-sed --default-names \
  gnupg \
  htop \
  hub \
  nmap \
  node \
  python \
  ripgrep \
  ruby \
  tldr \
  tmux \
  tree \
  vim \
  weechat \
  wget \
  yarn

brew install \
  universal-ctags/universal-ctags/universal-ctags --HEAD
