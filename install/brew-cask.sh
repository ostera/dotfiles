#!/bin/sh

brew cask install \
  firefox \
  google-chrome \
  iterm2 \
  monotype-skyfonts \
  sketch \
  spotify \
  steam \
  the-unarchiver \
  utorrent \
  vlc
